import os
import shlex
import subprocess
from time import sleep


def submit(command, requirements, dependencies, log, profile):
    command = 'python -m pype.commands --profile %s snippets --log %s %s' % (
        profile, log.__path__, command)
    log.log.info('Command: %s' % command)
    log.log.info('Requirements: %s' % requirements)
    log.log.info('Dependencies: %s' % dependencies)
    stdout = os.path.join(log.__path__, 'stdout')
    stderr = os.path.join(log.__path__, 'stderr')
    echo = command
    echo = shlex.split(echo)
    log.log.info('Writing stdin/stderr files into folder %s' % log.__path__)
    log.log.info('Executing command %s' % command)
    with open(stdout, 'wt') as std_out, open(stderr, 'wt') as std_err:
        echo_proc = subprocess.Popen(echo, stdout=std_out, stderr=std_err)
        out = echo_proc.communicate()[0]
    sleep(1)
