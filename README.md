# Sequenza pype modules

## Setup
install the [`bio_pype`](https://bitbucket.org/ffavero/bio_pype) python module with `pip install bio_pype`

Check the installation of modules
```
pype repo info
```
Setting the corresponding environment variable will customize the path of each module
(the default location is the python library path where `bio_pype` was installed)

## Cleanup modules

To make sure previously installed modules don't get in the way, cleanup the modules:

```
pype repos clean
```

## Installation

Select `sequenza` from the available repositories:

```
pype repos install -f sequenza
```

### Note

The pipeline requires external tools to be installed separately.
A list of the required tools is always available in the provided profile `yaml` file.

The required tools for sequenza are:

  * `samtools`
  * `tabix`
  * `R` (remeber to install the sequenza package within R)
  * [`sequenza-utils`](https://bitbucket.org/sequenzatools/sequenza-utils) python package

## Adjust the profile

Find the path of your profile

```
pype profiles info -p default
```

Edit the profile `yaml` file

```diff
 genome_build: any
 programs_load: path
 files:
-   genome_fa_gz: /databases/genome.fa.gz
-   genome_gc_wig: /databases/genome.gc50.wig.gz
+   genome_fa_gz: /path/of/your/fa.gz
+   genome_gc_wig: /path/of/your/gc.wig.gz
```

Check if everything is in place

```
pype profiles check default
```

## Sequenza pipeline

The pipeline will attempt to perform all the steps in parallel, using all the available resources.
To assign a specific number of CPUs or amount of memory use the `PYPE_NCPU` and `PYPE_MEM` environment variables.

```
pype pipelines sequenza
usage: pype pipelines sequenza [--ncpu NCPU] --sequenza_out SEQUENZA_OUT
                               --sample_name SAMPLE_NAME --seqz_out SEQZ_OUT
                               --tumor_bam TUMOR_BAM --normal_bam NORMAL_BAM
                               [--bin_size BIN_SIZE]

Required:
  Required pipeline arguments

  --sequenza_out SEQUENZA_OUT
                        Output path of the sequenza analysis, type: str
  --sample_name SAMPLE_NAME
                        Sample name, type: str
  --seqz_out SEQZ_OUT   Output name for the seqz file, type: str
  --tumor_bam TUMOR_BAM
                        Input Bam file of the tumor sample, type: str
  --normal_bam NORMAL_BAM
                        Input Bam file of the normal/control sample, type: str

Optional:
  Optional pipeline arguments

  --ncpu NCPU           Number of CPUs used for the model fitting, type: str.
                        Default: 4
  --bin_size BIN_SIZE   Binning size of the final seqz file, type: str.
                        Default: 50
```
